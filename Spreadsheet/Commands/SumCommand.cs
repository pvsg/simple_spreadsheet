﻿using Spreadsheet.Models;
using System;

namespace Spreadsheet.Commands
{
    public class SumCommand: ICommand
    {
        private readonly Func<ISpreadsheet> _getSpreadsheet;

        public SumCommand(Func<ISpreadsheet> getSpreadsheet)
        {
            _getSpreadsheet = getSpreadsheet;
        }

        public void Execute(string[] args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            if (args.Length < 6)
                throw new ArgumentException($"This command expects 6 arguments but only received {args.Length}");

            if (!int.TryParse(args[0], out var x1) || !int.TryParse(args[1], out var y1) ||
                !int.TryParse(args[2], out var x2) || !int.TryParse(args[3], out var y2) ||
                !int.TryParse(args[4], out var x3) || !int.TryParse(args[5], out var y3) 
                )
                throw new ArgumentException("There is some invalid arguments. All arguments should be integers");

            var spreadsheet = _getSpreadsheet();

            if (spreadsheet == null)
                throw new ArgumentException("No spread sheet exits. Please create one then try again.");

            spreadsheet.Sum(x1, y1, x2, y2, x3, y3);
        }
    }
}

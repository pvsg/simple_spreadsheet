﻿using System;
using Spreadsheet.Models;

namespace Spreadsheet.Commands
{
    public class InsertCommand : ICommand
    {
        private readonly Func<ISpreadsheet> _getSpreadsheet;

        public InsertCommand(Func<ISpreadsheet> getSpreadsheet)
        {
            _getSpreadsheet = getSpreadsheet;
        }

        public void Execute(string[] args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            if (args.Length < 3)
                throw new ArgumentException($"This command expects 3 arguments but only received {args.Length}");

            if (!int.TryParse(args[0], out var x) || !int.TryParse(args[1], out var y) || !int.TryParse(args[2], out var value))
                throw new ArgumentException("There is some invalid arguments. All arguments should be integers");

            var spreadsheet = _getSpreadsheet();

            if (spreadsheet == null)
                throw new ArgumentException("No spread sheet exits. Please create one then try again.");

            spreadsheet.SetValue(x, y, value);
        }
    }
}

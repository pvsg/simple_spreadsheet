﻿using System;
using Spreadsheet.Models;

namespace Spreadsheet.Commands
{
    public class CreateCommand : ICommand
    {
        private readonly Action<SimpleSpreadsheet> _callback;

        public CreateCommand(Action<SimpleSpreadsheet> callback)
        {
            _callback = callback;
        }

        public void Execute(string[] args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            if (args.Length < 2)
                throw new ArgumentException($"This command expects 2 arguments but only received {args.Length}");

            if (!uint.TryParse(args[0], out var width)
                || !uint.TryParse(args[1], out var height))
                throw new ArgumentException("There is some invalid arguments. Both arguments should be positive integers");

            _callback.Invoke(new SimpleSpreadsheet((int)width, (int)height));
        }
    }
}

﻿namespace Spreadsheet.Commands
{
    interface ICommand
    {
        void Execute(string[] args);
    }
}

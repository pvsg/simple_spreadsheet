﻿using System;
using System.Collections.Generic;
using Spreadsheet.Commands;
using Spreadsheet.Models;

namespace Spreadsheet
{
    class Program
    {
        private static SimpleSpreadsheet _spreadsheet;
        private static IDictionary<string, ICommand> _commands;

        private static void InitCommands()
        {
            _commands = new Dictionary<string, ICommand>
            {
                { "C", new CreateCommand(spreadsheet => _spreadsheet = spreadsheet) },
                { "N", new InsertCommand(() => _spreadsheet) },
                { "S", new SumCommand(() => _spreadsheet) },
                { "Q", new ExitCommand() }
            };
        }

        static void Main()
        {
            InitCommands();

            while (true)
            {
                Console.Write("enter command: ");
                var line = Console.ReadLine();
                if (line == null) break;

                var input = CommandParser.Parse(line);
                if(input == null) continue; //just ignore empty lines

                if (!_commands.ContainsKey(input.Command))
                {
                    Console.WriteLine("Error: Unknown command");
                    continue;
                }

                var command = _commands[input.Command];
                try
                {
                    command.Execute(input.Args);
                    Console.WriteLine(_spreadsheet.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }
            }
        }
    }
}

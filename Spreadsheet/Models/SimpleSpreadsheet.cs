﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Spreadsheet.Models
{
    public class SimpleSpreadsheet : ISpreadsheet
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public SimpleSpreadsheet(int width, int height)
        {
            Init(width, height);
        }

        private void Init(int width, int height)
        {
            if(width < 0 || height < 0) throw new ArgumentException();
            Width = width;
            Height = height;
            _buffer = Width <= 0 || Height <= 0 ? null : Enumerable.Repeat(0, Height * Width).ToArray();
        }

        public void SetValue(int x, int y, int value)
        {
            EnsureCellValid(x, y);
            SetValueUnsafe(x, y, value);
        }

        public int GetValue(int x, int y)
        {
            EnsureCellValid(x, y);
            return GetValueUnsafe(x, y);
        }

        public void Sum(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            EnsureCellValid(x1, y1);
            EnsureCellValid(x2, y2);
            EnsureCellValid(x3, y3);
            var sum = 0;
            for (var x = Math.Min(x1, x2); x <= Math.Max(x1, x2); ++x)
            {
                for (var y = Math.Min(y1, y2); y <= Math.Max(y1, y2); ++y)
                {
                    sum = sum + GetValueUnsafe(x, y);
                }
            }
            SetValueUnsafe(x3, y3, sum);
        }

        public override string ToString()
        {
            if (_buffer == null) return "<Empty>";
            var builder = new StringBuilder();
            builder.Append('-', Width * 3 + 2).AppendLine();
            for (var y = 1; y <= Height; ++y)
            {
                builder.Append('|');
                for (var x = 1; x <= Width; ++x)
                {
                    var value = GetValueUnsafe(x, y).ToString();
                    if (value.Equals("0")) value = "   ";
                    if (value.Length > 3) value = "???"; //TODO: replace with proper logic
                    value = value.PadLeft(3, ' ');
                    builder.Append(value);
                }
                builder.Append('|').AppendLine();
            }
            builder.Append('-', Width * 3 + 2).AppendLine();
            return builder.ToString();
        }

        [SuppressMessage("ReSharper", "ParameterOnlyUsedForPreconditionCheck.Local")]
        private void EnsureCellValid(int x, int y)
        {
            if (x < 1 || x > Width || y < 1 || y > Height) throw new ArgumentException();
        }

        private void SetValueUnsafe(int x, int y, int c)
        {
            _buffer[(y - 1) * Width + x - 1] = c;
        }
        private int GetValueUnsafe(int x, int y)
        {
            return _buffer[(y - 1) * Width + x - 1];
        }

        private int[] _buffer;
    }

}


﻿namespace Spreadsheet.Models
{
    public interface ISpreadsheet
    {
        int GetValue(int x, int y);
        void SetValue(int x, int y, int value);
        void Sum(int x1, int y1, int x2, int y2, int x3, int y3);

        int Height { get; }
        int Width { get; }
    }
}

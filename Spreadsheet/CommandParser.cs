﻿using System;
using System.Linq;

namespace Spreadsheet
{
    public class CommandParser
    {
        public static Input Parse(string rawInput)
        {
            char[] separators = { ' ', '\t', '\r', '\n' };
            var cmdArgs = rawInput.Split(separators, StringSplitOptions.RemoveEmptyEntries).ToList();
            return cmdArgs.Count < 1 ? null : new Input(cmdArgs[0], cmdArgs.Skip(1).ToArray());
        }
    }

    public class Input
    {
        public Input(string command, string[] args)
        {
            Command = command;
            Args = args;
        }

        public string Command { get; }
        public string[] Args { get; }
    }
}

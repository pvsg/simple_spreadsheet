# Simple Spreadsheet #

### Description of Test Assignment ###

You're given a task of writing a simple program where an end user will be able performing some basic spreadsheet operation (i.e. sum). 

In a nutshell, the program should work as follows:

* Create a new spread sheet  
* Add numbers in different cells and perform some calculation on top of specific row or column
* Quit  

Assume each cell will allocate at most 3 characters, thus numbers given here should be right justified accordingly.

| Command 		      | Description
----------------------|------------------------------------------------------------------------------
| C w h               | Should create a new spread sheet of width w and height h (i.e. the spreadsheet can hold w * h amount of cells).  
| N x1 y1 v1          | Should insert a number in specified cell (x1,y1).
| S x1 y1 x2 y2 x3 y3 | Should perform sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3.
| Q                   | Quit the program.  

Assume each cell will allocate at most 3 characters, thus numbers given here should be right justified accordingly.

### The Solution ###

I was using `Visual Studio 2017` to build and run main app and unit-tests.

The main code having no much complexity which may call for extra comments or documentation and usage is illustrated by unit-tests and main app itself. 
I was trying to balance and not to slip to over-design and over-commenting. 


The main app has no dependencies other than .NET.
The unittests projects have the following dependencies: `MSTest.TestAdapter` and `MSTest.TestFramework`


### Build and run ###

The main app and unit-tests can be built and ran in the usual way using Visual Studio.


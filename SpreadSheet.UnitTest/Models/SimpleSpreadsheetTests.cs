﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spreadsheet.Models;

namespace SpreadSheet.UnitTest.Models
{
    [TestClass()]
    public class SimpleSpreadsheetTests
    {
        [TestMethod()]
        public void SimpleSpreadsheetTest()
        {
            const int width = 20;
            const int height = 10;
            ISpreadsheet s = new SimpleSpreadsheet(width, height);
            Assert.AreEqual(width, s.Width);
            Assert.AreEqual(height, s.Height);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SimpleSpreadsheetTest_Exception()
        {
            const int width = -1;
            const int height = 10;
            ISpreadsheet s = new SimpleSpreadsheet(width, height);
            Assert.AreEqual(width, s.Width);
            Assert.AreEqual(height, s.Height);
        }

        [TestMethod()]
        public void SetValueTest()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(5, 5, 100);
            Assert.AreEqual(s.GetValue(3, 3), 0);
            Assert.AreEqual(s.GetValue(5, 5), 100);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SetValueTest_Exception()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(5, 500, 100);
        }

        [TestMethod()]
        public void SumTest()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(5, 5, 100);
            s.SetValue(3, 4, 200);
            s.Sum(1, 1, 6, 7, 9, 9);

            Assert.AreEqual(s.GetValue(5, 5), 100);
            Assert.AreEqual(s.GetValue(3, 4), 200);
            Assert.AreEqual(s.GetValue(9, 9), 300);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SumTest_Exception()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(5, 5, 100);
            s.SetValue(3, 4, 200);
            s.Sum(1, 1, 6, 7, 9, 567);
        }


        [TestMethod()]
        public void ToStringTest()
        {
            ISpreadsheet s = new SimpleSpreadsheet(7, 8);
            s.SetValue(5, 5, 100);
            s.SetValue(3, 4, 200);

            var builder = new StringBuilder();
            builder.AppendLine("-----------------------");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|      200            |");
            builder.AppendLine("|            100      |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("-----------------------");

            Assert.AreEqual(s.ToString(), builder.ToString());
        }

    }
}
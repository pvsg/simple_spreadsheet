﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spreadsheet.Commands;
using Spreadsheet.Models;

namespace SpreadSheet.UnitTest.Commands
{
    [TestClass()]
    public class CommandsTests
    {
        [TestMethod()]
        public void CreateCommandTest()
        {
            ISpreadsheet s = null;

            var cmd = new CreateCommand(spreadsheet => s = spreadsheet);
            cmd.Execute(new[] {"20", "10"});

            Assert.AreEqual(20, s.Width);
            Assert.AreEqual(10, s.Height);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateCommandTest_WrongArgsTypes()
        {
            // ReSharper disable once NotAccessedVariable
            ISpreadsheet s = null;

            var cmd = new CreateCommand(spreadsheet => s = spreadsheet);
            cmd.Execute(new[] { "20", "-2" });
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateCommandTest_WrongArgsNumber()
        {
            // ReSharper disable once NotAccessedVariable
            ISpreadsheet s = null;

            var cmd = new CreateCommand(spreadsheet => s = spreadsheet);
            cmd.Execute(new[] { "20" });
        }

        [TestMethod()]
        public void InsertCommandTest()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);

            var cmd = new InsertCommand(() => s);
            cmd.Execute(new[] { "3", "4", "200" });
            cmd.Execute(new[] { "5", "5", "100" });

            Assert.AreEqual(s.GetValue(3, 4), 200);
            Assert.AreEqual(s.GetValue(5, 5), 100);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertCommandTest_NoCanvas()
        {
            var cmd = new InsertCommand(() => null as SimpleSpreadsheet);
            cmd.Execute(new[] { "3", "4", "200" });
            cmd.Execute(new[] { "5", "5", "100" });
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertCommandTest_WrongArgsTypes()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            var cmd = new InsertCommand(() => s);
            cmd.Execute(new[] { "3", "S", "200" });
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertCommandTest_WrongArgsNumber()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            var cmd = new InsertCommand(() => s);
            cmd.Execute(new[] { "3", "3" });
        }

        [TestMethod()]
        public void SumCommandTest()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(3, 4, 100);
            s.SetValue(5, 5, 200);

            var cmd = new SumCommand(() => s);
            cmd.Execute(new[] { "2", "2", "7", "8", "9", "10" });

            Assert.AreEqual(s.GetValue(9, 10), 300);
        }

        [TestMethod()]
        public void SumCommandTest_ToString()
        {
            ISpreadsheet s = new SimpleSpreadsheet(7, 8);
            s.SetValue(5, 5, 100);
            s.SetValue(3, 4, 200);

            var cmd = new SumCommand(() => s);
            cmd.Execute(new[] { "2", "2", "7", "6", "3", "1" });

            var builder = new StringBuilder();
            builder.AppendLine("-----------------------");
            builder.AppendLine("|      300            |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|      200            |");
            builder.AppendLine("|            100      |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("|                     |");
            builder.AppendLine("-----------------------");

            Assert.AreEqual(s.ToString(), builder.ToString());
        }


        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SumCommandTest_NoCanvas()
        {
            var cmd = new SumCommand(() => null as SimpleSpreadsheet);
            cmd.Execute(new[] { "2", "2", "7", "8", "9", "10" });

        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SumCommandTest_WrongArgsTypes()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(3, 4, 100);
            s.SetValue(5, 5, 200);

            var cmd = new SumCommand(() => s);
            cmd.Execute(new[] { "2", "2", "7.0", "8", "9", "10" });

            Assert.AreEqual(s.GetValue(9, 10), 300);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SumCommandTest_WrongArgsNumber()
        {
            ISpreadsheet s = new SimpleSpreadsheet(20, 10);
            s.SetValue(3, 4, 100);
            s.SetValue(5, 5, 200);

            var cmd = new SumCommand(() => s);
            cmd.Execute(new[] { "2", "2", "7", "8", "9" });

            Assert.AreEqual(s.GetValue(9, 10), 300);
        }

    }
}